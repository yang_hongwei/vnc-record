#ifndef _ZLIB_TOOL_H_
#define _ZLIB_TOOL_H_

#include <vector>
#include <cstdint>
#include <sstream>
#include <zlib.h>

#define CHUNK 16384

class ZlibTool
{
public:
    ZlibTool();
    ~ZlibTool();

    void reset();

    std::vector<uint8_t> compress(const char *data, int len);
    std::vector<uint8_t> decompress(const char *data, int len);

private:
    std::stringstream m_compressStream;
    z_stream *m_strm;
};

#endif
