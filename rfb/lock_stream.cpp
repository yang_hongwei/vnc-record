#include "lock_stream.h"

#include <iostream>

void LockStream::write(const char *data, size_t size)
{
    m_lock.writeLock();
    try {
        m_stream.write(data, size);
    } catch (std::exception &e) {
        std::cout << "exception:" << e.what() << std::endl
                  << std::flush;
    } catch (...) {
        std::cout << "unknow exception!" << std::endl
                  << std::flush;
    }
    m_lock.writeUnlock();
}

void LockStream::read(char *data, size_t size)
{
    int len = 0;
    do {
        m_lock.readLock();
        try {
            len += m_stream.readsome(data + len, size - len);
        } catch (std::exception &e) {
            std::cout << "exception:" << e.what() << std::endl
                      << std::flush;
        } catch (...) {
            std::cout << "unknow exception!" << std::endl
                      << std::flush;
        }
        m_lock.readUnlock();
    } while (len != size);
}
