#ifndef _RFB_DATA_PPROCESS_H_
#define _RFB_DATA_PPROCESS_H_

#include "data_struct.h"
#include "lock_stream.h"

#include <thread>
#include <atomic>

class RfbDataProcesser
{
public:
    RfbDataProcesser();
    ~RfbDataProcesser();

    void writeServerData(const char *data, int len); // 写入服务器发送的数据
    void writeClientData(const char *data, int len); // 写入客户端发送的数据

private:
    void run();
    void printUpdateRectangleInfo(FramebufferUpdateRectangle rect);
    void readCursorTypeData(FramebufferUpdateRectangle rect, uint8_t bpp);

private:
    LockStream m_serverStream;
    LockStream m_clientStream;
    std::thread m_thread;
    std::atomic_bool m_clientNeedRecive;
};

#endif
