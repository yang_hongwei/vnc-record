#ifndef _RFB_MESSAGE_TYPE_
#define _RFB_MESSAGE_TYPE_

//--------------- 客户端到服务器的消息类型---------------------------------------
#define SetPixelFormatMsg 0 // 设置像素格式
#define SetEncodingsMsg 2 // 设置消息的编码格式
#define FramebufferUpdateRequestMsg 3 // 请求帧缓冲内容
#define KeyEventMsg 4 // 键盘事件消息
#define PointerEventMsg 5 // 鼠标事件消息
#define ClientCutTextMsg 6 // 剪切板消息
#define EnableContinuousUpdatesMsg 150 // 打开连续更新
#define ClientFenceMsg 248 // 客户端到服务端的数据同步请求
#define SetDesktopSizeMsg 251 // 客户端设置桌面大小
#define QEMUExtendedKeyEventMsg 255 // qumu虚拟机的扩展按键消息
//--------------- 服务器到客户端的消息类型---------------------------------------
#define FramebufferUpdateMsg 0 // 帧缓冲区更新消息
#define SetColorMapEntriesMsg 1 // 设置颜色地图
#define BellMsg 2 // 响铃
#define ServerCutTextMsg 3 // 设置剪切板数据
#define EndOfContinuousUpdatesMsg 150 // 结束连续更新
#define ServerFenceMsg 248 // 支持 Fence 扩展的服务器发送此扩展以请求数据流的同步

#endif