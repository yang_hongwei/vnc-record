#ifndef _DATA_STRUCT_H_
#define _DATA_STRUCT_H_

#include <cstdint>

struct s_rfbHeader {
    uint8_t protocol[3];
    uint8_t blank;
    uint8_t major[3];
    uint8_t pot;
    uint8_t minor[3];
    uint8_t br;
};

struct s_securityTypes {
    uint8_t num;
};

typedef struct pixelFormatStruct {
    uint8_t bpp; // bits per pixel
    uint8_t depth;
    uint8_t big_endian_flag;
    uint8_t true_color_flag;
    uint16_t red_max;
    uint16_t green_max;
    uint16_t blue_max;
    uint8_t red_shift;
    uint8_t green_shift;
    uint8_t blue_shift;
    uint8_t padding[3];
} s_pixelFormatStruct;

struct RFBServerInitStruct {
    uint16_t fbWidth;
    uint16_t fbHeight;
    pixelFormatStruct fbPixel;
    uint32_t fbNameLength;
};

struct FramebufferUpdate {
    int8_t padding;
    uint16_t number;
};

struct FramebufferUpdateRectangle {
    uint16_t xPos;
    uint16_t yPos;
    uint16_t width;
    uint16_t height;
    int32_t encodingType;
};

#endif