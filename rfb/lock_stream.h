#ifndef _LOCK_STREAM_H__
#define _LOCK_STREAM_H__

#include "common/rwlock.h"

#include <sstream>

class LockStream
{
public:
    void write(const char *data, size_t size);
    void read(char *data, size_t size);

private:
    std::stringstream m_stream;
    RWLock m_lock;
};

#endif
