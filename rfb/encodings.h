#ifndef _RFB_ENCODING_H_
#define _RFB_ENCODING_H_

//-----------编码类型-------------
#define EncRaw 0
#define EncCopyRect 1
#define EncRRE 2
#define EncHextile 5
#define EncTight 7 // tightvnc项目设置的编码
#define EncTRLE 15
#define EncZRLE 16
#define EncEndOfContinuousUpdates 150
#define EncCursor -239
#define EncDesktopSize -223

#endif