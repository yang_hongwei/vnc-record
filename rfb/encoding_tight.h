#ifndef _ENCODING_TIGHT_H_
#define _ENCODING_TIGHT_H_

#include "data_struct.h"
#include "lock_stream.h"
#include "zlib/zlib_tool.h"

namespace encoding {
namespace Tight {

class Encoder
{
public:
    std::vector<uint8_t> readData(LockStream &stream, struct FramebufferUpdateRectangle rect, s_pixelFormatStruct pixeFormat);

private:
    std::vector<uint8_t> readTightData(LockStream &stream, int dataSize, int decoderID);
    std::vector<uint8_t> handleTightFilters(LockStream &stream, struct FramebufferUpdateRectangle rect,
                                            s_pixelFormatStruct pixeFormat, uint8_t compressionControl);

private:
    ZlibTool m_zlibTool[4];
};

} // namespace Tight
} // namespace encoding

#endif
