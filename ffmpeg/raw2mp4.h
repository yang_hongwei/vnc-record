#ifndef _RAW2MP4_H_
#define _RAW2MP4_H_

#include "common/rwlock.h"

extern "C" {
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/opt.h>
}

#include <string>
#include <thread>
#include <atomic>

class Raw2Mp4Tool
{
public:
    Raw2Mp4Tool(int width, int height);
    bool open(std::string file);
    void close();
    void write(const uint8_t *rawData, int len);

private:
    void run();

private:
    std::string m_fileName;
    int m_width;
    int m_height;
    int m_srcWidth;
    int m_srcHeight;

    AVFormatContext *m_outContext;
    AVCodecContext *m_codecContext;
    SwsContext *m_ctx;
    AVFrame *m_yuv;
    int m_p;
    // 为保证视频帧数，单独保存，定时写入
    char *m_data;
    std::thread m_thread;
    std::atomic_bool m_open;
    RWLock m_lock;
};

#endif
